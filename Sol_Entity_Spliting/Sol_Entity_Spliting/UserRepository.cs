﻿using Sol_Entity_Spliting.EF;
using Sol_Entity_Spliting.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Entity_Spliting
{
    public class UserRepository
    {
        #region Declaration
        private UserDBEntities db = null;
        #endregion

        #region Constructor
        public UserRepository()
        {
            db = new UserDBEntities();
        }
        #endregion

        #region Public Methods
        public async Task<IEnumerable<UserEntity>> GetUserData(UserEntity userEntityObj)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;
           try
            {
                return await Task.Run(() => {

                    var getQuery =
                        db
                        .tblUsers
                            ?.AsEnumerable()
                            ?.Select(this.SelectData)
                            ?.ToList();

                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<UserEntity>> Search(UserEntity userEntityObj)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        db
                        .tblUsers
                            ?.AsEnumerable()
                            ?.Select(this.SelectData)
                            ?.ToList();

                    return getQuery;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tblUser,UserEntity> SelectData
        {
            get
            {
                return
                    (letblUserObj) => new UserEntity()
                    {
                        FirstName=letblUserObj.FirstName,
                        LastName=letblUserObj.LastName,
                        UserId=letblUserObj.UserId,
                        UserLogin=new UserLoginEntity()
                        {
                            UserName=letblUserObj.UserName,
                            Password=letblUserObj.Password,
                        },
                        UserCommunication=new UserCommunication()
                        {
                            EmailId=letblUserObj.EmailId,
                            MobileNo=letblUserObj.MobileNo
                        }
                    };
            }
        }
        #endregion
    }
}
